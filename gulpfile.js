/**
 * Created by smaudet on 1/10/17.
 */
var gulp = require('gulp')
  , watch = require('gulp-watch')
  , elm  = require('gulp-elm')
  , exec = require('child_process').exec
  , runSequence = require('run-sequence')
  ;

gulp.task('elm-init', elm.init);

gulp.task('stream', function () {
  // Endless stream mode
  return watch('*.elm', { ignoreInitial: false })
    .pipe(elm({filetype:"html"}))
    .pipe(gulp.dest('./'))
});

gulp.task('elm-reactor', function(){
  exec('elm-reactor', function (err, stdout, stderr) {
    console.log(stdout)
    console.log(stderr)
  })
})

gulp.task('default',function () {
  runSequence('elm-init',['stream','elm-reactor']);
})
